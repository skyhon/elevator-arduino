/*
  Steve Hon
  ESYS 57
  PLC Controlled Elevator
  Uses TB6600 Stepper Driver Unit
  Has speed control & reverse switch
  Last Update: December 5th, 2022
*/
// Constants
const bool UP = HIGH;
const bool DOWN = LOW;
// Define pins
const int DRV_PUL = 7;    // PUL-pin
const int DRV_DIR = 6;    // DIR-pin
const int PD = 500;       // Pulse Delay period, 500-1000, default: 750
const int ANALOG_PIN = A3; // Analog input from PLC's VO-0 Analog Output 
const int STEPS_PER_LEVEL = 950;  // will need to be calibrated for different structure
const int LS_CLEARANCE = 300; // steps between limit switch triggering point and level 1
const int EMERGENCY_PIN = 2;

enum ids {  // 
  calibration = 1, 
  // emergencyOn = 2,  // retired
  // emergencyOff = 3,  // retired
  lsOn = 4, 
  level1 = 5, 
  level2 = 6, 
  level3 = 7
};

// Variables
int currLevel = 1;
int prevLevel = 1;
int levelsBuffer[3];
bool isInBetweenLevels = false;
int remainingSteps = 0;
bool setdir = UP; // sets elevator direction, default: up or clockwise motor rotation
int prevId = 0;  // stores the previous id 
bool calibrated = false;

// get input value
int getInputId () { 
    int analogVal = analogRead(ANALOG_PIN);  // read input pin;
    Serial.print("analogVal: ");
    Serial.println(analogVal);
    return map(analogVal, 0, 1023, 1, 10);
}

// sets level to 1 and direction up
void reset() { 
  currLevel = 1;
  prevLevel = 1;
  setdir = UP;
  return;
}

// rotates a single "step"
// NEMA 17 stepper motor rotates at 200 steps/revolution or 1.8 deg/step.
void rotateOnce () { 
    digitalWrite(DRV_DIR,setdir);
    digitalWrite(DRV_PUL,HIGH);
    delayMicroseconds(PD);
    digitalWrite(DRV_PUL,LOW);
    delayMicroseconds(PD);
    return;
}

// move to initial level (level 1) after calibration
void initLevel () { 
  delay(500);
  for (int i = 0; i < LS_CLEARANCE; i++) 
    rotateOnce();
  return;
}

//calibrate - the elevator will go all the way down until it hits the limit switch
void calibrate () {
  Serial.println("Calibrating...");
  delay (3000);  // give 0.25 sec for PLC to change analog_out to "level 1"
  setdir = DOWN;
  int id;
  int level;
  do{
    id = getInputId(); 
    level = inputIdToLevel(id);
    if (level != -1)
      break;
    Serial.print ("calibrate: id = ");
    Serial.println(id); 
    rotateOnce();
  } while (id != lsOn);
  reset();
  initLevel();
  Serial.println("Calibration Complete!");
  calibrated = true;
  return;
}

// changes direction of the motor
void revmotor (){
  setdir = !setdir;
  return;
}

// convert mapped analog value to level 1, 2, 3, or -1
// -1 = invalid level
int inputIdToLevel(int inputId) { 
  int level = -1;
  switch (inputId) { 
    case level1:
      level = 1;
    break;
    case level2:
      level = 2;
    break;
    case level3:
      level = 3;
    break;
    default:
      level = -1;
    break;
  }
  return level;
  
} 

void goToLevel () { 
    Serial.print("Moving from level ");
    Serial.print (prevLevel);
    Serial.print(" to level ");
    Serial.print(currLevel);
    Serial.println(".  ");
    // lower or higher level?
    int deltaLevel = prevLevel - currLevel;
    int steps = abs (deltaLevel) * STEPS_PER_LEVEL;
    int id = -1;
    
    //Serial.print("Steps: ");
    //Serial.println(steps);  
    if (prevLevel > currLevel) {
      setdir = DOWN; 
    } else if (prevLevel < currLevel) {
      setdir = UP; 
    }
    Serial.print("Direction: ");
    Serial.println(setdir?"Clockwise/Up.  ":"Counter-Clockwise/Down.  ");   
    for (int i = 0; i < steps; i++) 
        rotateOnce();
    return;
}

// pause when emergency switch is triggered
void emergencyStop() { 
  int pause = digitalRead(EMERGENCY_PIN);
  while (pause) { 
    Serial.println("Emergency Stop");
    pause = digitalRead(EMERGENCY_PIN);
  }
  Serial.println("Resuming... ");
  return;
}

void setup() {
  pinMode (DRV_PUL, OUTPUT);
  pinMode (DRV_DIR, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(EMERGENCY_PIN), emergencyStop, RISING);
  Serial.begin(9600);
  Serial.println("Waiting for PLC to start");
  delay(10000);  // 10 seconds for PLC to startup
}

void loop() {
    int id = getInputId();
    int level = inputIdToLevel(id);
    if (level != -1) {  // level != -1 means one of the three levels (1, 2, or 3) is triggered
      currLevel = level;
      Serial.print("Previous and Current Level: ");
      Serial.print(prevLevel);
      Serial.print(" and ");
      Serial.println(currLevel);
      if (!calibrated && prevId == lsOn && id == level1)
        calibrate();
      if (prevLevel != currLevel) {
        // check if level already exist, yes->append, no->nothing
        goToLevel ();
        // pop level if any
        prevLevel = currLevel;
      } 
      calibrated = false;     
    } else {  // level == -1 means input other than controlling the level is triggered 
      Serial.print("ID: ");
      Serial.println(id);
      Serial.println("NOT a valid level");
      if (id == calibration)  
        calibrate();
    }
    prevId = id;
    delay(500);
}